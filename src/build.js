const plugin = {
  async install(Vue) {
    requireComponent.keys().forEach((fileName) => {
      const componentConfig = requireComponent(fileName);

      const componentName = fileName
        .split('/')
        .pop()
        .replace(/\.\w+$/, '');

      Vue.component(componentName, componentConfig.default || componentConfig);
    });

    this.EventBus = new Vue();
    Vue.prototype.$alert = {
      show(params) {
        plugin.EventBus.$emit('show', params);
      },
    };
  },
};

let GlobalVue = null;
if (typeof window !== 'undefined') {
  GlobalVue = window.Vue;
} else if (typeof global !== 'undefined') {
  GlobalVue = global.Vue;
}
if (GlobalVue) {
  GlobalVue.use(plugin);
}

const requireComponent = require.context('./components', false, /[\w-]+\.vue$/);

export default plugin;
