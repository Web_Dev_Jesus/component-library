module.exports = {
  module: {
    rules: [
      {
        test: /\.(png|jpg)$/i,
        loader: 'file-loader',
        options: {
          publicPath: 'assets',
        },
      },
    ],
  },
};
